# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

{
    "name": "Payroll Contract Advantages",
    "version": "2.0.3.0.0",
    "category": "Payroll",
    "website": "https://gitlab.com/flectra-community/payroll",
    "summary": "Allow to define contract advantages for employees.",
    "license": "LGPL-3",
    "author": "Nimarosa, Odoo Community Association (OCA)",
    "depends": ["hr_contract", "payroll"],
    "data": [
        "security/ir.model.access.csv",
        "views/hr_contract_advantage_views.xml",
        "views/hr_contract_views.xml",
    ],
    "application": True,
    "maintainers": ["nimarosa"],
}
