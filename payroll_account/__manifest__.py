# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.
{
    "name": "Payroll Accounting",
    "version": "2.0.2.1.2",
    "category": "Payroll",
    "website": "https://gitlab.com/flectra-community/payroll",
    "license": "LGPL-3",
    "summary": "Manage your payroll to accounting",
    "author": "Odoo SA, Odoo Community Association (OCA)",
    "depends": ["payroll", "account"],
    "data": ["views/hr_payroll_account_views.xml"],
    "demo": ["demo/hr_payroll_account_demo.xml"],
    "maintainers": ["appstogrow", "nimarosa"],
}
