# Copyright (C) 2021 Nimarosa (Nicolas Rodriguez) (<nicolasrsande@gmail.com>).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Payroll Public Holidays",
    "version": "2.0.2.0.0",
    "category": "Payroll",
    "website": "https://gitlab.com/flectra-community/payroll",
    "summary": "Integration between payroll and hr_public_holidays",
    "license": "AGPL-3",
    "author": "Nimarosa, Odoo Community Association (OCA)",
    "depends": ["payroll", "hr_holidays", "hr_holidays_public"],
    "data": [],
    "installable": True,
    "maintainers": ["nimarosa"],
}
