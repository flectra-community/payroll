# Flectra Community / payroll

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_payroll_period](hr_payroll_period/) | 2.0.1.1.0| Add payroll periods
[payroll_hr_public_holidays](payroll_hr_public_holidays/) | 2.0.2.0.0| Integration between payroll and hr_public_holidays
[payroll_account](payroll_account/) | 2.0.2.1.2| Manage your payroll to accounting
[payroll](payroll/) | 2.0.6.2.4| Manage your employee payroll records
[payroll_contract_advantages](payroll_contract_advantages/) | 2.0.3.0.0| Allow to define contract advantages for employees.
[payroll_rule_time_parameter](payroll_rule_time_parameter/) | 2.0.2.0.2| Payroll Rule Time Parameter


